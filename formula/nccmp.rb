class Nccmp < Formula
  desc "Compare two NetCDF files bitwise, semantically or with a tolerance"
  homepage "https://gitlab.com/remikz/nccmp"
  url "https://gitlab.com/remikz/nccmp/-/archive/1.9.1.0/nccmp-1.9.1.0.tar.gz"
  sha256 "5aa8d6cbc54d26f77e3d0511690cfafa57514a4145f75e8cabce782126509c91"

  depends_on "netcdf"

  def install
    system "./configure", "--disable-dependency-tracking",
                          "--disable-silent-rules",
                          "--prefix=#{prefix}"
    system "make"
    system "make", "install"
  end

  test do
    (testpath/"test.cdl").write("netcdf {}")
    system "ncgen", "-o", testpath/"test1.nc", testpath/"test.cdl"
    system "ncgen", "-o", testpath/"test2.nc", testpath/"test.cdl"
    assert_equal `#{bin}/nccmp -mds #{testpath}/test1.nc #{testpath}/test2.nc`,
                 "Files \"#{testpath}/test1.nc\" and \"#{testpath}/test2.nc\" are identical.\n"
  end
end
